import React from "react";
import { View, Text, TouchableHighlight, TextInput } from "react-native";
import styles from "./styles";

const InputWithButton = props => {
    const { buttonText, onPress, editable = true, textColor } = props;
    const containerStyles = [styles.container];
    const buttonTextStyles = [styles.buttonText];

    !editable && containerStyles.push(styles.containerDisabled);
    textColor && buttonTextStyles.push({ color: textColor });

    return (
        <View style={containerStyles}>
            <TouchableHighlight
                onPress={onPress}
                style={styles.buttonContainer}
                underlayColor="#f0f0f0"
            >
                <Text style={buttonTextStyles}>{buttonText}</Text>
            </TouchableHighlight>
            <View style={styles.border} />
            <TextInput style={styles.input} {...props} />
        </View>
    );
};

export default InputWithButton;
