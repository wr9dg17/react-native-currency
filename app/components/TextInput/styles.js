import { StyleSheet } from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";

const INPUT_HEIGHT = 48;
const BORDER_RADIUS = 4;

export default EStyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "$white",
        width: "90%",
        height: INPUT_HEIGHT,
        marginVertical: 12,
        borderRadius: BORDER_RADIUS
    },
    containerDisabled: {
        backgroundColor: "$lightGray"
    },
    buttonContainer: {
        alignItems: "center",
        justifyContent: "center",
        height: INPUT_HEIGHT,
        backgroundColor: "$white",
        borderTopLeftRadius: BORDER_RADIUS,
        borderBottomLeftRadius: BORDER_RADIUS
    },
    buttonText: {
        fontWeight: "500",
        fontSize: 18,
        color: "$primaryBlue",
        paddingHorizontal: 16
    },
    input: {
        flex: 1,
        fontSize: 15,
        height: INPUT_HEIGHT,
        paddingHorizontal: 10,
        color: "$inputText"
    },
    border: {
        height: INPUT_HEIGHT,
        width: StyleSheet.hairlineWidth,
        backgroundColor: "$border"
    }
});
