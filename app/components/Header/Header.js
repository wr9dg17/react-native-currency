import React from "react";
import { View, TouchableOpacity, Image } from "react-native";
import styles from "./styles";

const Header = ({ onPress }) => (
    <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Image
                source={require("../../../assets/images/gear.png")}
                resizeMode="contain"
                style={styles.icon}
            />
        </TouchableOpacity>
    </View>
);

export default Header;
