import EStylesheet from "react-native-extended-stylesheet";

export default EStylesheet.create({
    container: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        paddingTop: 25
    },
    button: {
        alignSelf: "flex-end",
        paddingVertical: 5,
        paddingHorizontal: 20
    },
    icon: {
        width: 20
    }
});
