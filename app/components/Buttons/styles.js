import EStylesheet from "react-native-extended-stylesheet";

export default EStylesheet.create({
    container: {
        alignItems: "center"
    },
    wrapper: {
        flexDirection: "row",
        alignItems: "center"
    },
    icon: {
        width: 20,
        marginRight: 10
    },
    text: {
        fontSize: 15,
        fontWeight: "300",
        color: "$white",
        paddingVertical: 20
    }
});
