import EStylesheet from "react-native-extended-stylesheet";
import { StyleSheet } from "react-native";

export default EStylesheet.create({
    $underlayColor: "$border",
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 20,
        paddingVertical: 16,
        backgroundColor: "$white"
    },
    text: {
        fontSize: 16,
        color: "$darkText"
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        marginLeft: 20,
        backgroundColor: "$border"
    },
    icon: {
        justifyContent: "center",
        alignItems: "center",
        width: 30,
        height: 30,
        backgroundColor: "transparent",
        borderRadius: 15
    },
    iconVisible: {
        backgroundColor: "$primaryBlue"
    },
    checkIcon: {
        width: 18
    }
});
