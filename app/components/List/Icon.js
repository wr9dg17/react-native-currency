import React from "react";
import { View, Image } from "react-native";
import styles from "./styles";

const Icon = ({ iconBackground, checkmark, visible }) => {
    const iconStyles = [styles.icon];
    if (visible) {
        iconStyles.push(styles.iconVisible);
    }

    if (iconBackground) {
        iconStyles.push({ backgroundColor: iconBackground });
    }

    return (
        <View style={iconStyles}>
            {checkmark ? (
                <Image
                    source={require("../../../assets/images/check.png")}
                    resizeMode="contain"
                    style={styles.checkIcon}
                />
            ) : null}
        </View>
    );
};

export default Icon;
