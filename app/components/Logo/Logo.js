import React, { Component } from "react";
import { View, Text, Keyboard, Animated, StyleSheet } from "react-native";
import styles from "./styles";

class Logo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            containerImageWidth: new Animated.Value(styles.$largeContainerSize),
            imageWidth: new Animated.Value(styles.$largeImageSize)
        };
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            "keyboardDidShow",
            this._keyboardDidShow
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            "keyboardDidHide",
            this._keyboardDidHide
        );
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow = () => {
        const { containerImageWidth, imageWidth } = this.state;

        Animated.parallel([
            Animated.timing(containerImageWidth, {
                toValue: styles.$smallContainerSize,
                duration: 200
            }),
            Animated.timing(imageWidth, {
                toValue: styles.$smallImageSize,
                duration: 200
            })
        ]).start();
    };

    _keyboardDidHide = () => {
        const { containerImageWidth, imageWidth } = this.state;

        Animated.parallel([
            Animated.timing(containerImageWidth, {
                toValue: styles.$largeContainerSize,
                duration: 200
            }),
            Animated.timing(imageWidth, {
                toValue: styles.$largeImageSize,
                duration: 200
            })
        ]).start();
    };

    render() {
        const { containerImageWidth, imageWidth } = this.state;
        const { tintColor } = this.props;

        const containerImageStyles = [
            styles.containerImage,
            { width: containerImageWidth, height: containerImageWidth }
        ];

        const imageStyles = [
            styles.logo,
            { width: imageWidth },
            tintColor ? { tintColor } : null
        ];

        return (
            <View style={styles.container}>
                <Animated.View style={containerImageStyles}>
                    <Animated.Image
                        source={require("../../../assets/images/background.png")}
                        resizeMode="contain"
                        style={[StyleSheet.absoluteFill, containerImageStyles]}
                    />
                    <Animated.Image
                        source={require("../../../assets/images/logo.png")}
                        resizeMode="contain"
                        style={imageStyles}
                    />
                </Animated.View>
                <Text style={styles.text}>Currency Converter</Text>
            </View>
        );
    }
}

export default Logo;
