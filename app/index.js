import React from "react";
import { Provider } from "react-redux";

import store from "./config/store";
import Navigator from "./config/routes";
import { AlertProvider } from "./components/Alert";
import EStyleSheet from "react-native-extended-stylesheet";

EStyleSheet.build({
    $primaryBlue: "#4f6d7a",
    $primaryOrange: "#d57866",
    $primaryGreen: "#00db9d",
    $primaryPurple: "#9e768f",

    $white: "#fff",
    $lightGray: "#f0f0f0",
    $inputText: "#797979",
    $border: "#e2e2e2",
    $darkText: "#343434"
});

export default () => (
    <Provider store={store}>
        <AlertProvider>
            <Navigator onNavigationStateChange={() => null} />
        </AlertProvider>
    </Provider>
);
