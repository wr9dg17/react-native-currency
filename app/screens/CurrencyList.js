import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { StatusBar, View, FlatList } from "react-native";

import { changeBaseCurrency, changeQuoteCurrency } from "../actions/currencies";
import { ListItem, Separator } from "../components/List";
import currencies from "../data/currencies";

class CurrencyList extends PureComponent {
    handlePress = currency => {
        const { type } = this.props.navigation.state.params;
        type === "base"
            ? this.props.dispatch(changeBaseCurrency(currency))
            : this.props.dispatch(changeQuoteCurrency(currency));

        this.props.navigation.goBack(null);
    };

    render() {
        const { baseCurrency, quoteCurrency, primaryColor } = this.props;
        let comparisonCurrency = baseCurrency;

        if (this.props.navigation.state.params.type === "quote") {
            comparisonCurrency = quoteCurrency;
        }

        return (
            <View style={{ flex: 1 }}>
                <StatusBar translucent={true} barStyle="light-content" />
                <FlatList
                    data={currencies}
                    renderItem={({ item }) => (
                        <ListItem
                            text={item}
                            selected={item === comparisonCurrency}
                            onPress={() => this.handlePress(item)}
                            iconBackground={primaryColor}
                        />
                    )}
                    keyExtractor={item => item}
                    ItemSeparatorComponent={Separator}
                />
            </View>
        );
    }
}

const mapStateToProps = state => ({
    baseCurrency: state.currencies.baseCurrency,
    quoteCurrency: state.currencies.quoteCurrency,
    primaryColor: state.theme.primaryColor
});

export default connect(mapStateToProps, null)(CurrencyList);
