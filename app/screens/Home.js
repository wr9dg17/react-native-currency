import React, { Component } from "react";
import { connect } from "react-redux";
import { StatusBar, KeyboardAvoidingView } from "react-native";

import { connectAlert } from "../components/Alert";
import { Container } from "../components/Container";
import { Header } from "../components/Header";
import { Logo } from "../components/Logo";
import { InputWithButton } from "../components/TextInput";
import { ClearButton } from "../components/Buttons";
import { LastConverted } from "../components/Text";

import {
    getInitialConversion,
    swapCurrency,
    changeCurrencyAmount
} from "../actions/currencies";

class Home extends Component {
    componentDidMount() {
        this.props.dispatch(getInitialConversion());
    }

    componentWillReceiveProps(nextProps) {
        if (
            nextProps.currencyError &&
            nextProps.currencyError !== this.props.currencyError
        ) {
            this.props.alertWithType("error", "Error!", nextProps.currencyError);
        }
    }

    handleOptionsPress = () => {
        this.props.navigation.navigate("Options", {
            title: "Options"
        });
    };

    handlePressBaseCurrency = () => {
        this.props.navigation.navigate("CurrencyList", {
            title: "Base Currency",
            type: "base"
        });
    };

    handlePressQuoteCurrency = () => {
        this.props.navigation.navigate("CurrencyList", {
            title: "Quote Currency",
            type: "quote"
        });
    };

    handleTextChange = text => {
        this.props.dispatch(changeCurrencyAmount(text));
    };

    handleSwapCurrency = () => {
        this.props.dispatch(swapCurrency());
    };

    render() {
        const {
            primaryColor,
            baseCurrency,
            quoteCurrency,
            amount,
            isFetching,
            conversionRate,
            lastConvertedDate
        } = this.props;

        let quotePrice = (amount * conversionRate).toFixed(2);

        if (isFetching) {
            quotePrice = "...";
        }

        return (
            <Container backgroundColor={primaryColor}>
                <StatusBar translucent={true} barStyle="light-content" />
                <Header onPress={this.handleOptionsPress} />
                <KeyboardAvoidingView behavior="padding">
                    <Logo tintColor={primaryColor} />
                    <InputWithButton
                        buttonText={baseCurrency}
                        onPress={this.handlePressBaseCurrency}
                        defaultValue={amount.toString()}
                        keyboardType="numeric"
                        onChangeText={this.handleTextChange}
                        textColor={primaryColor}
                    />
                    <InputWithButton
                        buttonText={quoteCurrency}
                        onPress={this.handlePressQuoteCurrency}
                        value={quotePrice}
                        editable={false}
                        textColor={primaryColor}
                    />
                    <LastConverted
                        base={baseCurrency}
                        quote={quoteCurrency}
                        conversionRate={conversionRate}
                        date={lastConvertedDate}
                    />
                    <ClearButton
                        text="Reverse Currencies"
                        onPress={this.handleSwapCurrency}
                    />
                </KeyboardAvoidingView>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    const primaryColor = state.theme.primaryColor;
    const {
        baseCurrency,
        quoteCurrency,
        amount,
        error: currencyError
    } = state.currencies;
    const conversionsSelector = state.currencies.conversions[baseCurrency] || {};
    const rates = conversionsSelector.rates || {};

    return {
        primaryColor,
        baseCurrency,
        quoteCurrency,
        amount,
        currencyError,
        isFetching: conversionsSelector.isFetching,
        conversionRate: rates[quoteCurrency] || 0,
        lastConvertedDate: conversionsSelector.date
            ? new Date(conversionsSelector.date)
            : new Date()
    };
};

export default connect(
    mapStateToProps,
    null
)(connectAlert(Home));
