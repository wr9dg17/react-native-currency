import {
    GET_INITIAL_CONVERSION,
    CHANGE_CURRENCY_AMOUNT,
    SWAP_CURRENCY,
    CHANGE_BASE_CURRENCY,
    CHANGE_QUOTE_CURRENCY
} from "./types";

export const getInitialConversion = () => ({
    type: GET_INITIAL_CONVERSION
});

export const changeCurrencyAmount = amount => ({
    type: CHANGE_CURRENCY_AMOUNT,
    amount: parseFloat(amount)
});

export const swapCurrency = () => ({
    type: SWAP_CURRENCY
});

export const changeBaseCurrency = currency => ({
    type: CHANGE_BASE_CURRENCY,
    currency
});

export const changeQuoteCurrency = currency => ({
    type: CHANGE_QUOTE_CURRENCY,
    currency
});
