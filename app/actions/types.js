export const GET_INITIAL_CONVERSION = "get_initial_conversion";
export const CHANGE_CURRENCY_AMOUNT = "change_currency_amount";
export const SWAP_CURRENCY = "swap_currency";
export const CHANGE_BASE_CURRENCY = "change_base_currency";
export const CHANGE_QUOTE_CURRENCY = "change_quote_currency";

export const CONVERSION_RESULT = "conversion_result";
export const CONVERSION_ERROR = "conversion_error";

export const CHANGE_PRIMARY_COLOR = "change_primary_color";
