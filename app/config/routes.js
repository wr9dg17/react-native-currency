import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { Animated } from "react-native";

import Home from "../screens/Home";
import CurrencyList from "../screens/CurrencyList";
import Options from "../screens/Options";
import Themes from "../screens/Themes";

const HomeStack = createStackNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                header: () => null
            }
        },
        Options: {
            screen: Options,
            navigationOptions: ({ navigation }) => ({
                headerTitle: navigation.state.params.title
            })
        },
        Themes: {
            screen: Themes,
            navigationOptions: ({ navigation }) => ({
                headerTitle: navigation.state.params.title
            })
        }
    },
    {
        transitionConfig: () => ({
            transitionSpec: {
                duration: 150,
                timing: Animated.timing
            }
        })
    }
);

const AppStack = createStackNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: {
                header: () => null
            }
        },
        CurrencyList: {
            screen: CurrencyList,
            navigationOptions: ({ navigation }) => ({
                headerTitle: navigation.state.params.title
            })
        }
    },
    {
        transitionConfig: () => ({
            transitionSpec: {
                duration: 150,
                timing: Animated.timing
            }
        })
    }
);

export default createAppContainer(AppStack);
