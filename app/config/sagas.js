import { takeEvery, select, call, put } from "redux-saga/effects";

import {
    GET_INITIAL_CONVERSION,
    CHANGE_BASE_CURRENCY,
    SWAP_CURRENCY,
    CONVERSION_RESULT,
    CONVERSION_ERROR
} from "../actions/types";

const getLatestRate = currency => {
    return fetch(`https://fixer.handlebarlabs.com/latest?base=${currency}`);
};

function* fetchLatestConversionRates(action) {
    try {
        let currency = action.currency;
        if (currency === undefined) {
            currency = yield select(state => state.currencies.baseCurrency);
        }

        const response = yield call(getLatestRate, currency);
        const result = yield response.json();

        if (result.error) {
            yield put({
                type: CONVERSION_ERROR,
                error: result.error
            });
        } else {
            yield put({
                type: CONVERSION_RESULT,
                result
            });
        }
    } catch (err) {
        yield put({
            type: CONVERSION_ERROR,
            error: err.message
        });
    }
}

export default function* rootSaga() {
    yield takeEvery(GET_INITIAL_CONVERSION, fetchLatestConversionRates);
    yield takeEvery(CHANGE_BASE_CURRENCY, fetchLatestConversionRates);
    yield takeEvery(SWAP_CURRENCY, fetchLatestConversionRates);
}
